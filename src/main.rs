extern crate rand;
extern crate read_input;

use rand::Rng;
use read_input::prelude::*;

fn main() {
    let mut rng = rand::thread_rng();
    let number: i32 = rng.gen_range(1, 100);
    let mut 
    is_correct: bool = false;
    //println!("DEBUG | the number is: {}", number);

    println!("I'm thinking of a nubmer, what is it?");

    while is_correct == false {
        let userin = input::<String>().get();

        if userin.parse::<i32>().unwrap() < number {
            println!("Higher!");
        } else if userin.parse::<i32>().unwrap() > number {
            println!("Lower!");
        } else if userin.parse::<i32>().unwrap() == number {
            println!("Correct!");
            is_correct = true;
            let end: String = input().get();
        }
    }
}
